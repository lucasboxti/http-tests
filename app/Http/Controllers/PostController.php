<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json([
            'data' => Post::all()
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $request->validate([
                'title' => 'required|string'
                ,'text' => 'required|string'
            ]);
    
            Post::create([
                'title' => $request->title
                ,'text' => $request->text
                ,'author' => $request->user()->id
            ]);   
        
        } catch (\Throwable $th) {
            return response()->json([
                'success' => false,
                'error' => $th->getMessage() 
            ], 400);
        }

        return response()->json([
            'success' => true
            ,'message' => 'Post criado com sucesso.'
        ], 201);        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        try {
            $request->validate([
                'title' => 'required|string'
                ,'text' => 'required|string'
            ]);
    
            $post->update([
                'title' => $request->title
                ,'text' => $request->text
            ]);   
        
        } catch (\Throwable $th) {
            return response()->json([
                'success' => false,
                'error' => $th->getMessage() 
            ], 400);
        }

        return response()->json([
            'success' => true
            ,'message' => 'Post alterado com sucesso.'
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        $post->delete();

        return response()->json([
            'success' => true,
            'message' => 'Post deletado com sucesso.'
        ], 200);
    }
}
