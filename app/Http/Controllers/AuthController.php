<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

class AuthController extends Controller
{

    public function register(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'email' => 'required|string|email|unique:users',
            'password' => 'required|string|confirmed',          
        ]);

        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password)           
        ]);

        return response()->json([
            'message' => 'Usuário criado com sucesso.'
        ], 201);
    }

    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string'
        ]);

        $credentials = [
            'email' => $request->email,
            'password' => $request->password
        ];

        $authenticated = Auth::attempt($credentials);

        if (!$authenticated) {
            return response()->json([
                'message' => 'Acesso negado'
            ], 401);
        }

        $user = $request->user();

        $token = $user->createToken('Token de acesso')->accessToken;

        return response()->json([
            'token' => $token,
            'user' => $user            
        ], 200);
    }

    public function logout(Request $request)
    {
        $request->user()->token()->revoke();

        return response()->json([
            'message' => 'Deslogado com sucesso'
        ], 200);
    }
}
