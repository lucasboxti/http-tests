<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json([
            'data' => Comment::all()
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $request->validate([
                'comment' => 'required|string'
                ,'post_id' => 'exists:App\Models\Post,id'
            ]);
    
            Comment::create([
                'comment' => $request->comment
                ,'post_id' => $request->post_id
                ,'author' => $request->user()->id
            ]);   
        
        } catch (\Throwable $th) {
            return response()->json([
                'success' => false,
                'error' => $th->getMessage() 
            ], 400);
        }

        return response()->json([
            'success' => true
            ,'message' => 'Comentário criado com sucesso.'
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function show(Comment $comment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function edit(Comment $comment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Comment $comment)
    {
        try {
            $request->validate([
                'comment' => 'required|string'
                ,'post_id' => 'exists:App\Models\Post,id'
            ]);
    
            $comment->update([
                'comment' => $request->comment
                ,'post_id' => $request->post_id
                ,'author' => $request->user()->id
            ]);   
        
        } catch (\Throwable $th) {
            return response()->json([
                'success' => false,
                'error' => $th->getMessage() 
            ], 400);
        }

        return response()->json([
            'success' => true
            ,'message' => 'Comentário alterado com sucesso.'
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Comment $comment)
    {
        $comment->delete();

        return response()->json([
            'success' => true,
            'message' => 'Comentário deletado com sucesso.'
        ], 200);
    }
}
