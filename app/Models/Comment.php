<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use HasFactory;

    protected $fillable = [
        'comment'
        ,'post_id'
        ,'author'
    ];

    protected $with = [
        'comment_author'
        ,'post'
    ];

    public function comment_author() {
        return $this->belongsTo('\App\Models\User', 'author');
    }

    public function post() {
        return $this->belongsTo('\App\Models\Post', 'post_id');
    }
}
