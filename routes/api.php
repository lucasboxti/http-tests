<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('login', [App\Http\Controllers\AuthController::class, 'login']);

Route::middleware('auth:api')->group(function () {

    Route::get('user', function (Request $request) {
        return response()->json([
            'data' => $request->user()
        ], 200);
    });

    Route::resource('post', App\Http\Controllers\PostController::class);
    Route::resource('comment', App\Http\Controllers\CommentController::class);
});
