<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserTest extends TestCase
{
    use DatabaseTransactions;
    /**
     * @test
     */
    public function getUserTest()
    {

        $user = \App\Models\User::factory()->create();

        $response = $this->actingAs($user, 'api')
            ->getJson('/api/user');

        $response
            ->assertJson([
                'data' => []
            ])
            ->assertStatus(200);
    }
}
