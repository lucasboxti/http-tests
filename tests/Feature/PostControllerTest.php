<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;
use App\Models\Post;

class PostControllerTest extends TestCase
{
    use DatabaseTransactions, WithFaker;
    /**
     * @test
     */
    public function postControllerIndexTestWhenAuthenticated()
    {        
        $user = User::factory()->create();

        $response = $this->actingAs($user, 'api')
            ->getJson('/api/post');

        $response
            ->assertJson([
                'data' => []
            ])
            ->assertStatus(200);
    }
    /**
     * @test
     */
    public function postControllerIndexTestWhenNotAuthenticated()
    {        
        $response = $this->getJson('/api/post');
        $response->assertStatus(401);
    }

    /**
     * @test
     */
    public function postControllerStoreTestWhenPassedValidData()
    {
        $user = User::factory()->create();       

        $response = $this->actingAs($user, 'api')
            ->postJson('/api/post', [
                'title' => $this->faker->sentence
                ,'text' => $this->faker->text
            ]);

        $response
            ->assertJson([
                'success' => true
                ,'message' => 'Post criado com sucesso.'
            ])
            ->assertStatus(201);
    }

    /**
     * @test
     */
    public function postControllerUpdateTestWhenPassedValidData()
    {
        $user = User::factory()->create();

        $post = Post::factory()->create();
        $titleToUpdate = $post->title;

        $titleUpdated = $this->faker->sentence;
    
        $response = $this->actingAs($user, 'api')
            ->putJson("/api/post/{$post->id}", [
                'title' => $titleUpdated
                ,'text' => $this->faker->text
            ]);

        $response
            ->assertJson([
                'success' => true
                ,'message' => 'Post alterado com sucesso.'
            ])
            ->assertStatus(200);
            // ->assertDatabaseHas('posts', [
            //     'title' => $titleUpdated
            // ])
            // ->assertDatabaseMissing('posts', [
            //     'title' => $titleToUpdate
            // ]);
    }
}
